package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.IUserService;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.repository.UserRepository;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public final class UserServiceTest {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final User user;

    @NotNull
    private final String userId;

    @NotNull
    final private String userLogin = "login";

    @Nullable
    final private String userPassword = "password";

    public UserServiceTest() throws AbstractException {
        user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        @NotNull final IUserRepository userRepository = new UserRepository();
        userRepository.add(user);
        propertyService = new PropertyService();
        userService = new UserService(userRepository, propertyService);
    }

    @Test
    public void create() throws AbstractException {
        Assert.assertEquals(1, userService.findAll().size());
        @NotNull final String newUserLogin = "newUserLogin";
        @NotNull final String newUserPassword = "newUserPassword";
        @NotNull final User newUser = userService.create(newUserLogin, newUserPassword);
        Assert.assertEquals(newUserLogin, newUser.getLogin());
        int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        Assert.assertEquals(HashUtil.salt(iteration, secret, newUserPassword), newUser.getPasswordHash());
    }

    @Test
    public void findByLogin() throws AbstractException {
        @NotNull final User tempUser = userService.findByLogin(userLogin);
        Assert.assertEquals(user, tempUser);
        Assert.assertEquals(userLogin, tempUser.getLogin());
        Assert.assertEquals(userPassword, tempUser.getPasswordHash());
    }

    @Test
    public void removeByLogin() throws AbstractException {
        Assert.assertFalse(userService.findAll().isEmpty());
        Assert.assertEquals(1, userService.findAll().size());
        userService.removeByLogin(userLogin);
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void setPassword() throws AbstractException {
        Assert.assertEquals(userPassword, user.getPasswordHash());
        @NotNull final String newPassword = "newPassword";
        userService.setPassword(userId, newPassword);
        Assert.assertNotEquals(userPassword, user.getPasswordHash());
        int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        Assert.assertEquals(HashUtil.salt(iteration, secret, newPassword), user.getPasswordHash());
    }

    @Test
    public void setRole() throws AbstractException {
        Assert.assertNotNull(user.getRole());
        Assert.assertEquals(user.getRole(), Role.USER);
        userService.setRole(userId, Role.ADMIN);
        Assert.assertEquals(user.getRole(), Role.ADMIN);
    }

    @Test
    public void updateById() throws AbstractException {
        @NotNull final String newLastName = "newLastName";
        @NotNull final String newFirstName = "newFirstName";
        @NotNull final String newMiddleName = "newMiddleName";
        @NotNull final String newEmail = "email";
        Assert.assertNull(user.getLastName());
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getMiddleName());
        Assert.assertNull(user.getEmail());
        userService.updateById(userId, newLastName, newFirstName, newMiddleName, newEmail);
        Assert.assertEquals(newLastName, user.getLastName());
        Assert.assertEquals(newFirstName, user.getFirstName());
        Assert.assertEquals(newMiddleName, user.getMiddleName());
        Assert.assertEquals(newEmail, user.getEmail());
    }

    @Test
    public void isLogin() throws AbstractException {
        Assert.assertTrue(userService.isLogin(userLogin));
    }

    @Test
    public void lockUnlockByLogin() throws AbstractException {
        Assert.assertFalse(user.isLocked());
        userService.lockByLogin(userLogin);
        Assert.assertTrue(user.isLocked());
        userService.unlockByLogin(userLogin);
        Assert.assertFalse(user.isLocked());
    }

}
