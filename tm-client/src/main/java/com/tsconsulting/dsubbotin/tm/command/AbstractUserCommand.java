package com.tsconsulting.dsubbotin.tm.command;

import com.tsconsulting.dsubbotin.tm.endpoint.User;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(@NotNull final User user) {
        TerminalUtil.printMessage("Id: " + user.getId() + "\n" +
                "Login: " + user.getLogin() + "\n" +
                "First Name: " + user.getFirstName() + "\n" +
                "Last Name: " + user.getLastName() + "\n" +
                "Middle Name: " + user.getMiddleName() + "\n" +
                "E-mail: " + user.getEmail() + "\n" +
                "Role: " + user.getRole().value()
        );
    }

    protected String showUserLine(@NotNull final User user) {
        return String.format("%s - Login: %s; Role: %s;",
                user.getId(),
                user.getLogin(),
                user.getRole()
        );
    }

}
