package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import org.jetbrains.annotations.Nullable;

public interface ISessionRepository {

    @Nullable
    Session getSession();

    void setSession(@Nullable final Session session);

}
