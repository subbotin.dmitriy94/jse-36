package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.endpoint.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserIsAuthCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-auth";
    }

    @Override
    @NotNull
    public String description() {
        return "Display authorized user.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        @NotNull final User user = endpointLocator.getUserEndpoint().getUser(session);
        showUser(user);
    }

}
